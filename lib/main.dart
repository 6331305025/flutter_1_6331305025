import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('More care'),
        ),
        body: Container(
            width: 1000,
            height: 1000,
            padding: const EdgeInsets.all(10),
            child: Column(children: [
              Container(
                width: 300,
                height: 100,
                alignment: Alignment.center,
                child: const Text(
                  /////หัว
                  "กระทู้สอบถาม",
                  style: TextStyle(fontSize: 34),
                ),
              ),
              const SizedBox(height: 30),
              Container(
                width: 350,
                height: 50,
                color: Color.fromRGBO(175, 242, 242, 1),
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("สมาชิก"),
                    const Text("หัวข้อ"),
                    const Text("เวลา")
                  ],
                ),
              ),
              const SizedBox(height: 10),
              Container(
                width: 350,
                height: 100,
                color: Color.fromRGBO(250, 230, 230, 1),
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image.network(
                      'https://www.appdisqus.com/wp-content/uploads/2021/01/u95dtycyesv51.png',
                      width: 70,
                      height: 70,
                    ),
                    const Text("ผมคิดว่าน้ำลายผมเป็นพิษครับ"),
                    const Text("10/2/2565")
                  ],
                ),
              ),
              const SizedBox(height: 5),
              Container(
                width: 350,
                height: 100,
                color: Color.fromRGBO(250, 230, 230, 1),
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image.network(
                      'https://www.appdisqus.com/wp-content/uploads/2021/01/u95dtycyesv51.png',
                      width: 70,
                      height: 70,
                    ),
                    const Text("ขาผมมันบอกว่า I like to move it"),
                    const Text("11/2/2565")
                  ],
                ),
              )
            ])));
  }
}
